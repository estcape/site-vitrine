FROM node:19-alpine3.15 AS build

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM node:19-alpine3.15

COPY package.json ./
COPY package-lock.json ./
COPY --from=build /app/build /app/build
EXPOSE 3000
CMD ["node", "/app/build"]
